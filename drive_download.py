import io
# import sys
import platform
import shutil
import argparse
import pathlib
import subprocess
import importlib.util

import google_auth_oauthlib.flow
from googleapiclient.http import MediaIoBaseDownload
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from google.auth.exceptions import RefreshError

TQDM_AVAILABLE = False
if importlib.util.find_spec("tqdm"):
    import tqdm
    TQDM_AVAILABLE = True

# if importlib.util.find_spec("tqdm"):
#     spec = importlib.util.find_spec("tqdm")
#     tqdm = importlib.util.module_from_spec(spec)
#     sys.modules["tqdm"] = tqdm
#     spec.loader.exec_module(tqdm)
#     TQDM_AVAILABLE = True


def copy2clip(data):
    result = None
    if platform.system() == "Darwin":
        result = subprocess.run("pbcopy", universal_newlines=True, input=data)
    elif platform.system() == "Windows":
        result = subprocess.run("clip", universal_newlines=True, input=data)
    else:
        print(f"Unsupported platform for clipboard pasting (platform: {platform.system()})")
    return result


class Auth:

    def __init__(self, client_secret_filename, scopes):
        self.client_secret = client_secret_filename
        self.scopes = scopes
        self.flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(self.client_secret, self.scopes)
        self.flow.redirect_uri = 'http://localhost:8080/'
        self.creds = None

    def get_credentials(self, run_local_server=True, auto_save=False, token_path=None):
        if not token_path:
            print("No token provided.")
            current_folder = pathlib.Path().resolve()
            default_token_path = current_folder / pathlib.Path("tokens/token.json")
            self.create_token(run_local_server, auto_save, default_token_path)
        elif not token_path.exists():
            print(f"No saved token credentials found at:\n   {token_path}")
            self.create_token(run_local_server, auto_save, token_path)
        else:
            print("Found saved token credentials...")
            self.creds = Credentials.from_authorized_user_file(token_path, self.scopes)

            if not self.creds or not self.creds.valid:
                print("Invalid token credentials")
                if self.creds and self.creds.expired and self.creds.refresh_token:
                    print("Trying to refresh the token...")
                    try:
                        self.creds.refresh(Request())
                    except RefreshError:
                        print("Could not refresh the token.")
                        self.create_token(run_local_server, auto_save, token_path)
                    else:
                        print("Token refreshed.")
                        if auto_save:
                            self.save_token(token_path)

        return self.creds

    def create_token(self, run_local_server=True, auto_save=True, token_path=None):
        # Documentation at:
        # https://google-auth-oauthlib.readthedocs.io/en/latest/reference/google_auth_oauthlib.flow.html

        print("Creating new credentials...")
        flow = InstalledAppFlow.from_client_secrets_file(self.client_secret,
                                                         self.scopes,
                                                         redirect_uri="urn:ietf:wg:oauth:2.0:oob")
        if run_local_server:
            self.creds = flow.run_local_server(port=8080)
        else:
            # self.creds = flow.run_console(authorization_prompt_message='Please visit this URL to authorize '
            #                                                            'this application: {url}',
            #                               authorization_code_message='Enter the authorization code: ')
            # Tell the user to go to the authorization URL.
            auth_url, _ = flow.authorization_url(prompt='consent')
            print(f"Please visit this URL to authorize this application:\n{auth_url}")
            result = copy2clip(auth_url)
            if result.returncode == 0:
                print("The URL has been automatically copied into the clipboard.")
            else:
                print("Could not automatically copy-paste the URL in the clipboard")
            # The user will get an authorization code. This code is used to get the access token.
            code = input("Enter the authorization code: ")
            flow.fetch_token(code=code)
            self.creds = flow.credentials

        print("Credentials created.")
        if auto_save:
            self.save_token(token_path)

    def save_token(self, token_path=pathlib.Path("./token.json")):
        token_path.parent.mkdir(parents=True, exist_ok=True)
        print(f"Saving credentials at:\n   {token_path}")
        # Save the credentials for the next run
        with open(token_path, 'w') as token:
            token.write(self.creds.to_json())


def search(service, query):
    # TODO: Search results for non-unique filenames with different file-ids
    # search for the file
    result = []
    page_token = None
    while True:
        response = service.files().list(q=query,
                                        spaces="drive",
                                        supportsAllDrives=True,
                                        includeItemsFromAllDrives=True,
                                        fields="nextPageToken, files(id, name, mimeType)",
                                        pageToken=page_token).execute()
        # iterate over filtered files
        for file in response.get("files", []):
            result.append((file["id"], file["name"], file["mimeType"]))
        page_token = response.get('nextPageToken', None)
        if not page_token:
            # no more files
            break

    return result


def download(drive_service, filename, output_filename="downloaded_file", folder=None):
    # The io_type controls if:
    # 1 - BytesIO: The file is first buffered in RAM and then dumped on the disk (can be faster for small files)
    # 2 - FileIO: The file directly written to the disk
    pbar = None
    io_type = 1

    # search for the file by name
    search_result = search(drive_service, query=f"name='{filename}'")
    if search_result:

        if folder:
            output_filename = folder / output_filename
        # get the GDrive ID of the file
        file_id = search_result[0][0]
        print("Downloading file '{}' (file_id={})".format(filename, file_id))

        # download file
        data_request = drive_service.files().get_media(fileId=file_id)
        meta_request = drive_service.files().get(fileId=file_id, fields="size").execute()
        file_size = int(meta_request["size"])

        # For small files (here <100Mb), we can use a buffered stream
        if file_size < 100 * 1000000:
            fh = io.BytesIO()
            io_type = 2
        else:
            fh = io.FileIO(output_filename, mode='w', opener=None)

        downloader = MediaIoBaseDownload(fh, data_request, chunksize=1024 * 1024)

        done = False
        buffer_pos = 0
        if TQDM_AVAILABLE:
            pbar = tqdm.tqdm(total=file_size, unit='B', unit_scale=True)

        while not done:
            status, done = downloader.next_chunk()
            if TQDM_AVAILABLE:
                pbar.set_postfix(file=filename, refresh=True)
                pbar.update(fh.tell() - buffer_pos)
            else:
                print(f"Download progress: {status.progress() * 100:.2f}%", end='\r')

            buffer_pos = fh.tell()

        if TQDM_AVAILABLE:
            pbar.close()
        else:
            print()

        if io_type == 2:
            # The file has been downloaded into RAM, now save it in a file
            fh.seek(0)
            with open(output_filename, 'wb') as f:
                # Default buffer-size is 1024 * 1024 if on Windows else 64 * 1024
                # Google documentation uses 2*64*1024=2^17
                shutil.copyfileobj(fh, f, length=131072)

        fh.close()

    else:
        print(f"'{filename}' not found in the Google Drive.")


def main():
    # The scope the app will use. 
    # (NEEDS to be among the enabled in the OAuth consent screen)

    token_path = None
    scopes = ["https://www.googleapis.com/auth/drive.metadata.readonly",
              "https://www.googleapis.com/auth/drive.readonly"]

    parser = argparse.ArgumentParser(description='Google Drive download interface script.')
    parser.add_argument('-f', '--filenames', nargs='+', help='filenames to download')
    parser.add_argument('-t', "--token-path", help="Path to a previously saved token or location where it will stored",
                        type=pathlib.Path)
    parser.add_argument('-l', "--file-list-path", help="Path to a list of files to be downloaded",
                        type=pathlib.Path)
    parser.add_argument('-d', "--folder-destination", help="Path to a folder destination",
                        type=pathlib.Path)
    args = parser.parse_args()

    if args.filenames or args.file_list_path:

        if args.token_path:
            token_path = args.token_path.resolve()

        credentials = Auth(client_secret_filename="credentials.json", scopes=scopes) \
            .get_credentials(run_local_server=False,
                            auto_save=True,
                            token_path=token_path)

        drive_service = build('drive', 'v3', credentials=credentials)

        if args.folder_destination:
            args.folder_destination.mkdir(parents=True, exist_ok=True)

        if args.filenames:
            for filename in args.filenames:
                download(drive_service, filename, output_filename=filename, folder=args.folder_destination)
        elif args.file_list_path:
            file_path = args.file_list_path
            if file_path.exists():
                with open(file_path, 'r') as f:
                    filenames = f.read().splitlines()
                    for filename in filenames:
                        download(drive_service, filename, output_filename=filename, folder=args.folder_destination)
            else: 
                print(f"File '{file_path}' not found.")

    else:
        print("You need to provide either:\n  - a list of filenames (-f)\n  - a path to a file containing a list of files (-l) (with a \\n delimiter).")


if __name__ == '__main__':
    main()
