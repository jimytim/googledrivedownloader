## Google Drive Python API QuickStart

This script demonstrates the use of the Google API through the Python SDK, in order to download a file from a user's Google Drive. It is based on the official [tutorial](https://developers.google.com/drive/api/quickstart/python).

---
## Getting Started

The code runs with Python 3. A [conda](https://docs.conda.io/en/latest/miniconda.html#installing) environment file is provided. 


### Prerequisites

To get access to the Google APIs, your application needs to have its own credentials. You need create them from the [Google Cloud platform](https://cloud.google.com/) as well as setting up your Google OAuth client. 


### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

1. Clone the repo
   ```sh
   git clone https://gitlab.com/jimytim/googledrivedownloader.git
   ``` 
      
2. Install python packages
   ```sh
   conda env create -f google_api_environment.yml
   ```
3. Activate the environment
   ```sh
   conda activate googleapi
   ```
4. Move your Google application credentials as `credentials.json` in the root directory

___
## Usage

_Use the -h flag to get more info on the script options._

Example:
The scripts takes either:
* a list of filenames as paramaters
```sh
   python drive_download.py -f "MyFile1.txt" "MyFile2.txt"
   ```
* a path to a file containing a list of filenames (with a \\n delimiter)
```sh
   python drive_download.py -l "filesList.txt"
   ```

By default, files are downloaded in the current directory, you can specify a destination folder with (it will be created if not existing).
```sh
   python drive_download.py -l "filesList.txt" -d "./Downloads"
   ```

### Token creation process

The first time, the script generates an authentication url (and copies it automatically pastes it in the clipboard) and waits for the authorization code in order to request a token.

Steps:
1. Open a web-browser and paste the url.
2. Login to your account (it has to be one of your registered test users if it is not a domain google account and your app has not been validated yet).
3. Grant the app access to the requested scopes.
4. Copy the authorization code from the web page in the terminal.
5. A token is created and stored in the current script folder in a `tokens/` directory. It can be reused later like this:
```sh
   python drive_download.py "MyFile.txt" -t ./tokens/token.json
   ```
6. The download process begins.

___
## Notes
### File search:
The file search use only the filename and supports shared drive. But it does not take in account folder hierarchy. This can result in wrong files being downloaded if their share the same names.
### File download:
Download process has the following features:
* Files are downloaded in chunks.
* For small files (<100Mb), a buffered stream is used.
* For bigger files, the file is directly dumped on the disk.
* If available, the progress is displayed using `tqdm`.
<p align="right">(<a href="#readme-top">back to top</a>)</p>

